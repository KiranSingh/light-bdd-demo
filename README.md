# AcroLoans

### A limited API to demo the usage of [Light BDD](https://github.com/LightBDD/LightBDD)

A set of Light BDD (Api.AcceptanceTests project - with mocked out dependencies) and some unit tests validate adding of a loan if:

* The customer Id & loan manager Id are valid
* The loan manager has not got more than maximum allowed active customers
* The loan value, duration & interest rate are within required limits 

### Acceptance.Api tests run the Light BDD with a Sqlite Database to validate integration. 

## Important: 
* .Net Core SDK version 3.1 will be needed to run the project.