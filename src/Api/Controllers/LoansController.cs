using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    public class LoansController : Controller
    {
        public const string ErrorMessageValidIdRequired = "A valid id is required for this call";
        
        private readonly IMapper _mapper;
        private readonly IService<Loan> _service;

        public LoansController(IMapper mapper, IService<Loan> service)
        {
            _mapper = mapper;
            _service = service;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id < 1)
                return BadRequest(ErrorMessageValidIdRequired);

            var loan = await _service.Get(id);

            return new OkObjectResult(loan);
        }
        
        [HttpGet]
        public async Task<IActionResult> List() => Ok(await _service.All());

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddLoanRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var loan = _mapper.Map<AddLoanRequest, Loan>(request);

            var id = await _service.Add(loan);

            loan.Id = id;

            return CreatedAtAction(nameof(Get), new {id = id}, loan);
        }
    }
}