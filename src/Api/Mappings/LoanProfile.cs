using AcroLoans.Common;
using AcroLoans.Models;
using AutoMapper;

namespace Api.Mappings
{
    public class LoanProfile : Profile
    {
        public LoanProfile()
        {
            CreateMap<AddLoanRequest, CustomerValidationModel>()
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.CustomerId))
                .ForMember(dest => dest.LoanManagerId, opt => opt.MapFrom(src => src.LoanManagerId))
                .ForMember(dest => dest.MaxActiveCustomersAllowed,
                    opt => opt.MapFrom<MaxActiveCustomersAllowedResolver>());
                
            CreateMap<AddLoanRequest, Loan>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.CustomerId))
                .ForMember(dest => dest.Deleted, opt => opt.Ignore())
                .ForMember(dest => dest.DurationWeeks, opt => opt.MapFrom(src => src.DurationWeeks))
                .ForMember(dest => dest.InterestRate, opt => opt.MapFrom(src => src.InterestRate))
                .ForMember(dest => dest.LoanManagerId, opt => opt.MapFrom(src => src.LoanManagerId))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value));
        }
    }
}