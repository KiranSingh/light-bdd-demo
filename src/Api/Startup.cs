﻿using System;
using System.Linq;
using AcroLoans.Common;
using AcroLoans.Models;
using AcroLoans.Repositories;
using AcroLoans.Repositories.Entities;
using AcroLoans.Services;
using Api.Validators;
using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) => Configuration = configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddControllersAsServices()
                .AddJsonOptions(options => options.JsonSerializerOptions.IgnoreNullValues = true)
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Description =
                        "Limited API to showcase Light BDD. Customers have Ids 1 to 10 while Managers have Ids 1, 2 & 3 with 10 loans already added, including max(3) for Manager Id 3.",
                    Title = "Acro Loans API",
                    Version = "v1",
                });
            });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            var loanConfig = new LoanConfiguration();
            Configuration.Bind("Loan", loanConfig);
            services.TryAddSingleton(loanConfig);

            if (services.All(d => d.ServiceType != typeof(AcroLoansContext)))
            {
                loanConfig.DbFilePath.CreateFileIfNotExists();
                
                var builder = new SqliteConnectionStringBuilder
                {
                    DataSource = loanConfig.DbFilePath,
                    Mode = SqliteOpenMode.ReadWrite,
                    Cache = SqliteCacheMode.Default,
                };

                try
                {
                    var connection = new SqliteConnection(builder.ConnectionString);
                    connection.Open();
                    connection.EnableExtensions(true);

                    services.AddEntityFrameworkSqlite()
                        .AddDbContext<AcroLoansContext>(options => options.UseSqlite(connection));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            
            services.TryAddScoped<IRepository<Customer>, Repository<Customer, CustomerEntity>>();
            
            services.TryAddScoped<IRepository<Loan>, Repository<Loan, LoanEntity>>();

            services.TryAddScoped<IReadOnlyRepository<LoanManager>, LoanManagerRepository>();

            services.TryAddScoped<IService<Customer>, ServiceBase<Customer>>();
            
            services.TryAddScoped<IService<Loan>, LoanService>();

            services.TryAddScoped(sp => sp.GetService<AcroLoansContext>().LoanManagers);
            
            services.TryAddScoped<ILoanManagerService, LoanManagerService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMapper autoMapper)
        {
            autoMapper.ConfigurationProvider.AssertConfigurationIsValid();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseRouting();
            
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());
        }
    }
}