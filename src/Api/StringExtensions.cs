using System.IO;

namespace Api
{
    public static class StringExtensions
    {
        public static void CreateFileIfNotExists(this string filePath)
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath);
            }
        }
    }
}