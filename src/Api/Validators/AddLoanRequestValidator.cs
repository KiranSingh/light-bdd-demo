using System.Threading;
using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using AcroLoans.Services;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;

namespace Api.Validators
{
    public class AddLoanRequestValidator : AbstractValidator<AddLoanRequest>
    {
        public const string FormatInvalidIdForEntity = "{0} is not an Id of a valid {1}";
        public const string FormatLoanManagerFullyExtended = "The Loan Manager Id {0} is not allowed to take on more customers";

        private readonly IService<Customer> _customerService;
        private readonly ILoanManagerService _loanManagerService;
        private readonly IMapper _mapper;

        public AddLoanRequestValidator(IService<Customer> customerService, 
            ILoanManagerService loanManagerService, 
            IMapper mapper, LoanConfiguration config)
        {    
            _customerService = customerService;
            _loanManagerService = loanManagerService;
            _mapper = mapper;

            
            RuleFor(x => x.CustomerId).MustAsync(ExistInCustomerDatabase)
                .WithMessage(x => string.Format(FormatInvalidIdForEntity, x.CustomerId, nameof(Customer)));

            RuleFor(x => x.LoanManagerId).MustAsync(ExistInLoanManagerDatabase)
                .WithMessage(x => string.Format(FormatInvalidIdForEntity, x.LoanManagerId, nameof(LoanManager)))
                .DependentRules(() =>
                    WhenAsync(async (request, token) => await ExistInCustomerDatabase(request.CustomerId, token),
                        () => RuleFor(x => x).MustAsync(HaveManagerAvailableForLoans).WithMessage(x =>
                            string.Format(FormatLoanManagerFullyExtended, x.LoanManagerId))));

            RuleFor(x => x.DurationWeeks)
                .GreaterThanOrEqualTo(config.MinLoanDurationWeeks)
                .WithMessage($"The Loan Duration cannot be less than {config.MinLoanDurationWeeks} weeks");

            RuleFor(x => x.InterestRate)
                .GreaterThanOrEqualTo(config.MinLoanInterestRate)
                .WithMessage($"The Loan Interest Rate cannot be less than {config.MinLoanInterestRate}%");

            RuleFor(x => x.Value)
                .GreaterThanOrEqualTo(config.MinLoanValue)
                .WithMessage($"The Loan Value cannot be less than £{config.MinLoanValue}");
        }

        protected override bool PreValidate(ValidationContext<AddLoanRequest> context, ValidationResult result)
        {
            if (context.InstanceToValidate != null) 
                return true;
            
            result.Errors.Add(new ValidationFailure("", "A valid AddLoanRequest is required for this call."));
            return false;
        }

        private async Task<bool> ExistInCustomerDatabase(int customerId, CancellationToken arg2)
        {
            return await _customerService.Exists(customerId);
        }

        private async Task<bool> ExistInLoanManagerDatabase(int loanManagerId, CancellationToken arg2)
        {
            return await _loanManagerService.Exists(loanManagerId);
        }

        private async Task<bool> HaveManagerAvailableForLoans(AddLoanRequest model, CancellationToken arg2)
        {
            var customerValidationModel = _mapper.Map<AddLoanRequest, CustomerValidationModel>(model);

            var availableForLoans = await _loanManagerService.AllowedToAddLoanFor(customerValidationModel);

            return availableForLoans;
        }
    }
}