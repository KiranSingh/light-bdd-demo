namespace AcroLoans.Common
{
    public interface IId
    {
        int Id { get; set; }

        bool Deleted { get; set; }
    }
}