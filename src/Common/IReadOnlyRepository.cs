using System.Threading.Tasks;

namespace AcroLoans.Common
{
    public interface IReadOnlyRepository<T> where T : class, IId
    {
        Task<T[]> All();

        Task<bool> Exists(int id);

        Task<T> Get(int id);
    }
}