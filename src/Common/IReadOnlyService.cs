using System.Threading.Tasks;

namespace AcroLoans.Common
{
    public interface IReadOnlyService<T> where T : class, IId
    {
        Task<bool> Exists(int id);
        
        Task<T> Get(int id);
    }
}