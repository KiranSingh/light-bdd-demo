using System.Threading.Tasks;

namespace AcroLoans.Common
{
    public interface IRepository<T> : IReadOnlyRepository<T> where T : class, IId
    {
        Task<int> Add(T item);
        
        Task SaveChanges();
    }
}