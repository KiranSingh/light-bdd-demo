using System.Threading.Tasks;

namespace AcroLoans.Common
{
    public interface IService<T> : IReadOnlyService<T> where T : class, IId
    {
        Task<int> Add(T item);

        Task<T[]> All();
        
        Task Save();
    }
}