namespace AcroLoans.Common
{
    public class LoanConfiguration
    {
        public string DbFilePath { get; set; }

        public int MaxActiveCustomersAllowed { get; set; }

        public int MinLoanDurationWeeks { get; set; }

        public double MinLoanInterestRate { get; set; }

        public double MinLoanValue { get; set; }
    }
}