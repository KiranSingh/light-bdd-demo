using AutoMapper;

namespace AcroLoans.Common
{
    public class MaxActiveCustomersAllowedResolver : IValueResolver<object, object, int>
    {
        private readonly LoanConfiguration _loanConfiguration;

        public MaxActiveCustomersAllowedResolver(LoanConfiguration loanConfiguration)
        {
            _loanConfiguration = loanConfiguration;
        }

        public int Resolve(
            object source, object destination,
            int destMember, ResolutionContext context)
        {
            return _loanConfiguration.MaxActiveCustomersAllowed;
        }
    }
}