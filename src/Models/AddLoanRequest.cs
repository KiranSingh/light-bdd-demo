using System;

namespace AcroLoans.Models
{
    public class AddLoanRequest
    {
        private DateTime _startDate;

        public virtual int CustomerId { get; set; }

        public int LoanManagerId { get; set; }

        public int DurationWeeks { get; set; }

        public double InterestRate { get; set; }

        public DateTime StartDate
        {
            get => _startDate == DateTime.MinValue ? DateTime.Today : _startDate;
            set => _startDate = value;
        }

        public double Value { get; set; }
    }
}