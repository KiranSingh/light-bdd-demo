namespace AcroLoans.Models
{
    public class CustomerValidationModel
    {
        public int CustomerId { get; set; }

        public virtual int LoanManagerId { get; set; }
        
        public int MaxActiveCustomersAllowed { get; set; }
    }
}