using System;
using AcroLoans.Common;

namespace AcroLoans.Models
{
    public class Loan : IId
    {
        public virtual int Id { get; set; }
        
        public virtual int CustomerId { get; set; }
        
        public bool Deleted { get; set; }

        public int DurationWeeks { get; set; }

        public double InterestRate { get; set; }

        public int LoanManagerId { get; set; }
        
        public DateTime StartDate { get; set; }

        public double Value { get; set; }

        public virtual bool Active()
        {
            return DateTime.Now >= StartDate && DateTime.Today <= StartDate.AddDays(DurationWeeks * 7);
        }
    }
}