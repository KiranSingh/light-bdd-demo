using System.Collections.Generic;
using System.Linq;

namespace AcroLoans.Models
{
    public class LoanManager : Person
    {
        public virtual IList<int> ActiveCustomerIds =>
            Loans?.Where(x => x.Active()).Select(x => x.CustomerId).Distinct().ToList();

        public virtual bool AllowedToAddLoanFor(CustomerValidationModel model)
        {
            var activeCustomerIds = ActiveCustomerIds;

            return activeCustomerIds == null || 
                (activeCustomerIds.Contains(model.CustomerId) ||
                 activeCustomerIds.Count < model.MaxActiveCustomersAllowed);
        }
    }
}