using System;
using System.Collections.Generic;
using AcroLoans.Common;

namespace AcroLoans.Models
{
    public abstract class Person : IId
    {
        public int Id { get; set; }

        public DateTime DateOfBirth { get; set; }

        public bool Deleted { get; set; }

        public string Forename { get; set; }

        public string Initials { get; set; }

        public string Surname { get; set; }

        public string Title { get; set; }

        public virtual ICollection<Loan> Loans { get; set; }
    }
}