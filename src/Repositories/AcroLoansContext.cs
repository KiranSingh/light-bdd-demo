using System;
using System.Runtime.CompilerServices;
using AcroLoans.Repositories.Entities;
using Microsoft.EntityFrameworkCore;

[assembly:InternalsVisibleTo("AcroLoans.Api.AcceptanceTests")]
namespace AcroLoans.Repositories
{
    public class AcroLoansContext : DbContext
    {
        private readonly Random _random = new Random();

        internal AcroLoansContext()
        {
            throw new NotImplementedException();
        }

        public AcroLoansContext(DbContextOptions<AcroLoansContext> options) : base(options)
        {
        }

        public DbSet<CustomerEntity> Customers { get; set; }

        public DbSet<LoanEntity> Loans { get; set; }

        public DbSet<LoanManagerEntity> LoanManagers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerEntity>()
                .ToTable("Customer")
                .Property(x => x.Id);

            modelBuilder.Entity<LoanEntity>()
                .ToTable("Loan")
                .Property(x => x.Id)
                .IsRequired()
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<LoanManagerEntity>()
                .ToTable("LoanManager")
                .Property(x => x.Id);

            modelBuilder.Entity<CustomerEntity>()
                .HasMany(x => x.Loans)
                .WithOne(x => x.Customer);

            modelBuilder.Entity<LoanManagerEntity>()
                .HasMany(x => x.Loans)
                .WithOne(x => x.LoanManager);
        }

        public void ClearTables()
        {
            Loans.RemoveRange(Loans);
            LoanManagers.RemoveRange(LoanManagers);
            Customers.RemoveRange(Customers);
        }

        public void Seed()
        {
            Customers.AddRange(
                new CustomerEntity
                {
                    Id = 1, DateOfBirth = RandomDate(), Forename = "John", Initials = "D", Surname = "Smith",
                    Title = "Mr"
                },
                new CustomerEntity
                    {Id = 2, DateOfBirth = RandomDate(), Forename = "Liz", Surname = "Painter", Title = "Mrs"},
                new CustomerEntity
                    {Id = 3, DateOfBirth = RandomDate(), Forename = "Jo", Surname = "Johnson", Title = "Mr"},
                new CustomerEntity
                    {Id = 4, DateOfBirth = RandomDate(), Forename = "Wai", Surname = "Lung", Title = "Mr"},
                new CustomerEntity
                    {Id = 5, DateOfBirth = RandomDate(), Forename = "Preeti", Surname = "Patel", Title = "Ms"},
                new CustomerEntity
                    {Id = 6, DateOfBirth = RandomDate(), Forename = "Emma", Surname = "Thompson", Title = "Mrs"},
                new CustomerEntity
                    {Id = 7, DateOfBirth = RandomDate(), Forename = "Jessica", Surname = "Alba", Title = "Ms"},
                new CustomerEntity
                    {Id = 8, DateOfBirth = RandomDate(), Forename = "Tom", Surname = "Cruise", Title = "Mr"},
                new CustomerEntity
                    {Id = 9, DateOfBirth = RandomDate(), Forename = "Liv", Surname = "Tailor", Title = "Ms"},
                new CustomerEntity
                    {Id = 10, DateOfBirth = RandomDate(), Forename = "Jason", Surname = "Bourne", Title = "Mr"});

            LoanManagers.AddRange(
                new LoanManagerEntity
                    {Id = 1, DateOfBirth = RandomDate(), Forename = "Mason", Surname = "Clive", Title = "Mr"},
                new LoanManagerEntity
                    {Id = 2, DateOfBirth = RandomDate(), Forename = "Shakura", Surname = "Jenny", Title = "Ms"},
                new LoanManagerEntity
                    {Id = 3, DateOfBirth = RandomDate(), Forename = "Molly", Surname = "M", Title = "Ms"});

            Loans.AddRange(
                new LoanEntity
                {
                    DurationWeeks = 24, InterestRate = 2.56, Value = 1000, StartDate = DateTime.Today.AddDays(-8 * 7),
                    CustomerId = 1, LoanManagerId = 1, Id  = 1001,
                },
                new LoanEntity
                {
                    DurationWeeks = 14, InterestRate = 2.23, Value = 1500, StartDate = DateTime.Today.AddDays(-5 * 7),
                    CustomerId = 1, LoanManagerId = 1, Id  = 1002,
                },
                new LoanEntity
                {
                    DurationWeeks = 24, InterestRate = 3.45, Value = 3000, StartDate = DateTime.Today.AddDays(-2 * 7),
                    CustomerId = 2, LoanManagerId = 2, Id  = 1003,
                },
                new LoanEntity
                {
                    DurationWeeks = 45, InterestRate = 1.45, Value = 1000, StartDate = DateTime.Today.AddDays(-28 * 7),
                    CustomerId = 2, LoanManagerId = 2, Id  = 1004,
                },
                new LoanEntity
                {
                    DurationWeeks = 36, InterestRate = 3.45, Value = 1000, StartDate = DateTime.Today.AddDays(-48 * 7),
                    CustomerId = 5, LoanManagerId = 3, Id  = 1005,
                },
                new LoanEntity
                {
                    DurationWeeks = 48, InterestRate = 3.76, Value = 3000, StartDate = DateTime.Today.AddDays(-58 * 7),
                    CustomerId = 2, LoanManagerId = 3, Id  = 1006,
                },
                new LoanEntity
                {
                    DurationWeeks = 48, InterestRate = 2.76, Value = 4000, StartDate = DateTime.Today.AddDays(-26 * 7),
                    CustomerId = 2, LoanManagerId = 3, Id  = 1007,
                },
                new LoanEntity
                {
                    DurationWeeks = 94, InterestRate = 4.78, Value = 2000, StartDate = DateTime.Today.AddDays(-82 * 7),
                    CustomerId = 3, LoanManagerId = 3, Id  = 1008,
                },
                new LoanEntity
                {
                    DurationWeeks = 55, InterestRate = 3.87, Value = 1500, StartDate = DateTime.Today.AddDays(-22 * 7),
                    CustomerId = 4, LoanManagerId = 3, Id  = 1009,
                },
                new LoanEntity
                {
                    DurationWeeks = 45, InterestRate = 2.67, Value = 4500, StartDate = DateTime.Today.AddDays(-27 * 7),
                    CustomerId = 5, LoanManagerId = 3, Id  = 1010,
                });

            SaveChanges();
        }

        private DateTime RandomDate() =>
            new DateTime(_random.Next(1950, 2000), _random.Next(1, 12), _random.Next(1, 28));
    }
}