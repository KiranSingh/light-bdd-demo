using System;
using AcroLoans.Common;

namespace AcroLoans.Repositories.Entities
{
    public class LoanEntity : IId
    {
        public int Id { get; set; }

        public bool Deleted { get; set; }

        public int DurationWeeks { get; set; }

        public double InterestRate { get; set; }

        public DateTime StartDate { get; set; }

        public double Value { get; set; }

        public int CustomerId { get; set; }

        public CustomerEntity Customer { get; set; }

        public int LoanManagerId { get; set; }

        public LoanManagerEntity LoanManager { get; set; }
    }
}