using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AcroLoans.Common;

namespace AcroLoans.Repositories.Entities
{
    public abstract class PersonEntity : IId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool Deleted { get; set; }

        public DateTime DateOfBirth { get; set; }

        [Required]
        [StringLength(50)]
        public string Forename { get; set; }

        public string Initials { get; set; }

        [Required]
        [StringLength(50)]
        public string Surname { get; set; }

        [Required]
        [StringLength(10)]
        public string Title { get; set; }

        public virtual ICollection<LoanEntity> Loans { get; set; }
    }}