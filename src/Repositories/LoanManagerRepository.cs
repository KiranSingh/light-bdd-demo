using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using AcroLoans.Repositories.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AcroLoans.Repositories
{
    public class LoanManagerRepository : ReadOnlyRepository<LoanManager, LoanManagerEntity>
    {
        private readonly DbSet<LoanManagerEntity> _dbSet;
        private readonly IMapper _mapper;

        public LoanManagerRepository(DbSet<LoanManagerEntity> dbSet, IMapper mapper) : base(dbSet, mapper)
        {
            _dbSet = dbSet;
            _mapper = mapper;
        }

        public override async Task<LoanManager> Get(int id)
        {
            var loanManagerEntity =
                await _dbSet
                    .Include("Loans")
                    .FirstOrDefaultAsync(x => x.Id == id);

            var loanManager = _mapper.Map<LoanManagerEntity, LoanManager>(loanManagerEntity);

            return loanManager;
        }
    }
}