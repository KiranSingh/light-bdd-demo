using AcroLoans.Models;
using AcroLoans.Repositories.Entities;
using AutoMapper;

namespace AcroLoans.Repositories
{
    public class LoanProfile : Profile
    {
        public LoanProfile()
        {
            CreateMap<LoanEntity, Loan>();
            
            CreateMap<Loan, LoanEntity>()
                .ForMember(dest => dest.Customer, opt => opt.Ignore())
                .ForMember(dest => dest.LoanManager, opt => opt.Ignore());

            CreateMap<LoanManager, LoanManagerEntity>()
                .ForMember(dest => dest.Loans, opt => opt.Ignore());

            CreateMap<LoanManagerEntity, LoanManager>()
                .IgnoreAllPropertiesWithAnInaccessibleSetter();
        }
    }
}