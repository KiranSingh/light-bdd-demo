using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AcroLoans.Common;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AcroLoans.Repositories
{
    public class ReadOnlyRepository<T, K> : IReadOnlyRepository<T> where T : class, IId where K : class, IId
    {
        protected readonly DbSet<K> DbSet;
        protected readonly IMapper Mapper;

        protected ReadOnlyRepository(DbSet<K> dbSet, IMapper mapper)
        {
            DbSet = dbSet;
            Mapper = mapper;
        }
        
        public virtual async Task<T> Get(int id)
        {
            var entity = await DbSet.FirstOrDefaultAsync(x => x.Id == id);

            return entity == null ? default(T) : Mapper.Map<K, T>(entity);
        }

        public async Task<T[]> All()
        {
            var entities = await DbSet.ToListAsync();

            return entities == null || !entities.Any() ? new T[] { } : Mapper.Map<List<K>, T[]>(entities);
        }
        
        public virtual async Task<bool> Exists(int id)
        {
            var entity = await DbSet.FirstOrDefaultAsync(x => x.Id == id);
            return entity != null;
        }
    }
}