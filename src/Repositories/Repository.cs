using System.Threading.Tasks;
using AcroLoans.Common;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AcroLoans.Repositories
{
    public class Repository<T, K> : ReadOnlyRepository<T, K>, IRepository<T> where T : class, IId where K : class, IId
    {
        private readonly AcroLoansContext _dbContext;

        public Repository(AcroLoansContext dbContext, IMapper mapper) : base(dbContext.Set<K>(), mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<int> Add(T item)
        {
            var entity = Mapper.Map<T, K>(item);

            await DbSet.AddAsync(entity);

            await SaveChanges();

            return entity.Id;
        }

        public async Task SaveChanges()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}