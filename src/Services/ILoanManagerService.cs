using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;

namespace AcroLoans.Services
{
    public interface ILoanManagerService : IReadOnlyService<LoanManager>
    {
        Task<bool> AllowedToAddLoanFor(CustomerValidationModel model);
    }
}