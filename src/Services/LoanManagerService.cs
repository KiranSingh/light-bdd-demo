using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;

namespace AcroLoans.Services
{
    public class LoanManagerService : ReadOnlyServiceBase<LoanManager>, ILoanManagerService
    {
        private readonly IReadOnlyRepository<LoanManager> _readOnlyRepository;

        public LoanManagerService(IReadOnlyRepository<LoanManager> repository) : base(repository)
        {
            _readOnlyRepository = repository;
        }

        public async Task<bool> AllowedToAddLoanFor(CustomerValidationModel model)
        {
            var loanManager = await _readOnlyRepository.Get(model.LoanManagerId);

            return loanManager.AllowedToAddLoanFor(model);
        }
    }
}