using AcroLoans.Common;
using AcroLoans.Models;
using AutoMapper;

namespace AcroLoans.Services
{
    public class LoanProfile : Profile
    {
        public LoanProfile()
        {
            CreateMap<Loan, CustomerValidationModel>()
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.CustomerId))
                .ForMember(dest => dest.LoanManagerId, opt => opt.MapFrom(src => src.LoanManagerId))
                .ForMember(dest => dest.MaxActiveCustomersAllowed,
                    opt => opt.MapFrom<MaxActiveCustomersAllowedResolver>());
        }
    }
}