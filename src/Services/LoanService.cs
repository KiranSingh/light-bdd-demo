using System;
using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using AutoMapper;

namespace AcroLoans.Services
{
    public class LoanService : ServiceBase<Loan>
    {
        private readonly ILoanManagerService _loanManagerService;
        private readonly IMapper _mapper;

        public LoanService(IRepository<Loan> repository, ILoanManagerService loanManagerService, IMapper mapper) : base(
            repository)
        {
            _loanManagerService = loanManagerService;
            _mapper = mapper;
        }
        
        public override async Task<int> Add(Loan item)
        {
            var customerValidationModel = _mapper.Map<Loan, CustomerValidationModel>(item);
            
            if (!await _loanManagerService.AllowedToAddLoanFor(customerValidationModel))
            {
                throw new ApplicationException(
                    $"The Loan Manager Id {customerValidationModel.LoanManagerId} cannot take on new customers");
            }

            return await base.Add(item);
        }
    }
}