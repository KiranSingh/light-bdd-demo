using System.Threading.Tasks;
using AcroLoans.Common;

namespace AcroLoans.Services
{
    public abstract class ReadOnlyServiceBase<T> : IReadOnlyService<T> where T : class, IId
    {
        private readonly IReadOnlyRepository<T> _readOnlyRepository;

        protected ReadOnlyServiceBase(IReadOnlyRepository<T> repository)
        {
            _readOnlyRepository = repository;
        }
        
        public async Task<bool> Exists(int id)
        {
            return await _readOnlyRepository.Exists(id);
        }

        public async Task<T> Get(int id)
        {
            return await _readOnlyRepository.Get(id);
        }
    }
}