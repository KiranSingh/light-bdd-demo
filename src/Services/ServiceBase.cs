using System.Threading.Tasks;
using AcroLoans.Common;

namespace AcroLoans.Services
{
    public class ServiceBase<T> : ReadOnlyServiceBase<T>, IService<T> where T : class, IId
    {
        private readonly IRepository<T> _repository;

        public ServiceBase(IRepository<T> repository) : base(repository)
        {
            _repository = repository;
        }

        public virtual async Task<int> Add(T item)
        {
            return await _repository.Add(item);
        }

        public async Task<T[]> All() => await _repository.All();

        public async Task Save()
        {
            await _repository.SaveChanges();
        }
    }
}