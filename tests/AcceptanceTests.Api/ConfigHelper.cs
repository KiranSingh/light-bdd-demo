
using Microsoft.Extensions.Configuration;

namespace AcroLoans.AcceptanceTests.Api
{
    public class ConfigHelper
    {
        public static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile("appsettings.Development.json", false, true)
                .Build();
        }
    }
}