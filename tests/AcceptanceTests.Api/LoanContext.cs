using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using AcroLoans.Repositories;
using AcroLoans.Repositories.Entities;
using Api;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace AcroLoans.AcceptanceTests.Api
{
    public class LoanContext
    {
        private readonly Fixture _fixture = new Fixture();

        private AddLoanRequest _addLoanRequest;
        private HttpResponseMessage _response;
        private int _loanId;
        private readonly LoanConfiguration _loanConfiguration;
        private readonly IConfigurationRoot _configurationRoot;
        private readonly SqliteConnectionStringBuilder _builder;
        private TestServer _testServer;

        public LoanContext()
        {
            _loanConfiguration = new LoanConfiguration();
            _configurationRoot = ConfigHelper.GetConfiguration();
            _loanConfiguration = new LoanConfiguration();
            _configurationRoot.Bind("Loan", _loanConfiguration);

            _loanConfiguration.DbFilePath.CreateFileIfNotExists();
            
            _builder = new SqliteConnectionStringBuilder
            {
                DataSource = _loanConfiguration.DbFilePath, 
                Mode = SqliteOpenMode.ReadWrite,
                Cache = SqliteCacheMode.Default,
            };
        }

        public async Task Given_valid_AddLoanRequest()
        {
            _addLoanRequest = new AddLoanRequest
            {
                CustomerId = _fixture.Create<int>(),
                DurationWeeks = _loanConfiguration.MinLoanDurationWeeks + _fixture.Create<int>(),
                InterestRate = _loanConfiguration.MinLoanInterestRate + _fixture.Create<double>(),
                LoanManagerId = _fixture.Create<int>(),
                StartDate = DateTime.Today.AddDays(_fixture.Create<int>()),
                Value = _loanConfiguration.MinLoanValue + _fixture.Create<double>(),
            };
            
            _testServer = await _builder.TestServerWithData(
                _configurationRoot, _addLoanRequest.CustomerId,
                _addLoanRequest.LoanManagerId);
        }

        public async Task When_request_is_received()
        {
            var client = _testServer.CreateClient();

            _response = await client.PostAsJsonAsync("/api/loans", _addLoanRequest);
        }

        public async Task Then_new_loan_added_for_customer()
        {
            var result = await _builder.LoansFromDatabase();

            result.Count.Should().Be(1);
            result[0].CustomerId.Should().Be(_addLoanRequest.CustomerId);
            result[0].Deleted.Should().BeFalse();
            result[0].DurationWeeks.Should().Be(_addLoanRequest.DurationWeeks);
            result[0].InterestRate.Should().Be(_addLoanRequest.InterestRate);
            result[0].LoanManagerId.Should().Be(_addLoanRequest.LoanManagerId);
            result[0].StartDate.Should().Be(_addLoanRequest.StartDate);
            result[0].Value.Should().Be(_addLoanRequest.Value);

            _loanId = result[0].Id;
        }

        public async Task Then_valid_loan_created_response_is_returned()
        {
            _response.StatusCode.Should().Be(HttpStatusCode.Created);

            _response.Headers.Location.AbsoluteUri.ToLower().Should().Be($"http://localhost/api/loans/{_loanId}");

            var content = await _response.Content.ReadAsStringAsync();

            var loan = JsonConvert.DeserializeObject<Loan>(content);
            loan.Id.Should().Be(_loanId);
            loan.CustomerId.Should().Be(_addLoanRequest.CustomerId);
            loan.Deleted.Should().BeFalse();
            loan.DurationWeeks.Should().Be(_addLoanRequest.DurationWeeks);
            loan.InterestRate.Should().Be(_addLoanRequest.InterestRate);
            loan.LoanManagerId.Should().Be(_addLoanRequest.LoanManagerId);
            loan.StartDate.Should().Be(_addLoanRequest.StartDate);
            loan.Value.Should().Be(_addLoanRequest.Value);
        }

        public async Task Given_AddLoanRequest_with_invalid_CustomerId(int customerId)
        {
            await Given_valid_AddLoanRequest();

            _addLoanRequest.CustomerId = customerId;
            
            _testServer = await _builder.TestServerWithData(
                _configurationRoot, 0,
                _addLoanRequest.LoanManagerId);

        }

        public async Task When_invalid_request_is_received()
        {
            var client = _testServer.CreateClient();

            _response = await client.PostAsJsonAsync("/api/loans", _addLoanRequest);
        }

        public async Task Then_Bad_Request_response_is_returned_with_error_message(string errorMessage, int loanCount)
        {
            var result = await _builder.LoansFromDatabase();
            
            result.Count.Should().Be(loanCount < 0 ? _loanConfiguration.MaxActiveCustomersAllowed : loanCount);
            
            _response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var response = _response.Content.ReadAsStringAsync().Result;
            response.Should().Contain(errorMessage);
        }

        public async Task Given_AddLoanRequest_with_invalid_LoanManagerId(int loanManagerId)
        {
            await Given_valid_AddLoanRequest();
            _addLoanRequest.LoanManagerId = loanManagerId;

            _testServer = await _builder.TestServerWithData(
                _configurationRoot, _addLoanRequest.CustomerId,
                0);
        }
        
        public async Task Given_AddLoanRequest_with_invalid_property(string propertyName, object propertyValue)
        {
            await Given_valid_AddLoanRequest();
            typeof(AddLoanRequest).GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public)
                .SetValue(_addLoanRequest, propertyValue);
        }
        
        public async Task Given_AddLoanRequest_for_LoanManager_fully_extended(int loanManagerId)
        {
            await Given_valid_AddLoanRequest();
            _addLoanRequest.LoanManagerId = loanManagerId;

            var testServer = _builder.TestServer(_configurationRoot);

            using var scope = testServer.Host.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<AcroLoansContext>();

            await context.Database.OpenConnectionAsync();
            await context.Database.EnsureCreatedAsync();

            context.ClearTables();

            var activeCustomers = Enumerable.Range(1, _loanConfiguration.MaxActiveCustomersAllowed).ToList()
                .Select(x => _fixture.Build<CustomerEntity>()
                    .Without(y => y.Loans)
                    .Create())
                .ToList();
                
            await context.Customers.AddRangeAsync(activeCustomers);

            await context.Customers.AddAsync(_fixture.Build<CustomerEntity>()
                .Without(y => y.Loans)
                .With(y => y.Id, _addLoanRequest.CustomerId)
                .Create());

            await context.LoanManagers.AddAsync(new LoanManagerEntity
            {
                Id = loanManagerId,
                DateOfBirth = DateTime.Today.AddYears(-42).AddDays(-125),
                Forename = "Jackie",
                Surname = "Mandel",
                Title = "Mrs",
                Loans = activeCustomers.Select(x =>
                {
                    var days = _fixture.Create<int>();
                        
                    return new LoanEntity
                    {
                        CustomerId = x.Id,
                        DurationWeeks = days/7 + 5,
                        LoanManagerId = loanManagerId,
                        StartDate = DateTime.Today.AddDays(-days),
                    };
                }).ToList()
            });
                
            await context.SaveChangesAsync();
        }
    }
}