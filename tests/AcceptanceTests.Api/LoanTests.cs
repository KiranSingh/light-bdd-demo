using System.Threading.Tasks;
using AcroLoans.Models;
using Api.Validators;
using AutoFixture;
using LightBDD.Framework;
using LightBDD.Framework.Scenarios;
using LightBDD.XUnit2;
using Xunit;

namespace AcroLoans.AcceptanceTests.Api
{
    [FeatureDescription("Add loan for a customer")]
    public class LoanTests : FeatureFixture
    {
        private readonly Fixture _fixture = new Fixture();

        [Label("PS100")]
        [Scenario(DisplayName = "Valid loan added to valid customer")]
        [ScenarioCategory("Functional")]
        public async Task Valid_Loan_added_to_valid_Customer()
        {
            await Runner.WithContext<LoanContext>()
                .AddAsyncSteps(
                    
                    _ => _.Given_valid_AddLoanRequest(),
                    
                    _ => _.When_request_is_received()
                )
                .AddAsyncSteps(
                    
                    _ => _.Then_new_loan_added_for_customer(),
                    
                    _ => _.Then_valid_loan_created_response_is_returned()
                )
                .RunAsync();
        }

        [Label("PS-3001")]
        [Scenario(DisplayName = "Bad request returned if Loan Manager already has max active customers")]
        [ScenarioCategory("Validation")]
        public async Task Bad_request_returned_if_LoanManager_already_has_max_active_customers()
        {
            var loanManagerId = _fixture.Create<int>();

            await Runner.WithContext<LoanContext>()
                .AddAsyncSteps(
                    
                    _ => _.Given_AddLoanRequest_for_LoanManager_fully_extended(loanManagerId),
                    
                    _ => _.When_invalid_request_is_received(),
                    
                    _ => _.Then_Bad_Request_response_is_returned_with_error_message(
                        string.Format(AddLoanRequestValidator.FormatLoanManagerFullyExtended, loanManagerId), -1)
                )
                .RunAsync();
        }
        
        [Label("PS-3002")]
        [Scenario(DisplayName = "Bad request returned if AddLoanRequest has invalid CustomerId")]
        [ScenarioCategory("Validation")]
        public async Task Bad_request_returned_if_AddLoanRequest_has_invalid_CustomerId()
        {
            var customerId = _fixture.Create<int>();

            await Runner.WithContext<LoanContext>()
                .AddAsyncSteps(
                    
                    _ => _.Given_AddLoanRequest_with_invalid_CustomerId(customerId),
                    
                    _ => _.When_invalid_request_is_received(),
                
                    _ => _.Then_Bad_Request_response_is_returned_with_error_message(
                        string.Format(AddLoanRequestValidator.FormatInvalidIdForEntity, customerId, nameof(Customer)), 0))
                .RunAsync();
        }

        [Label("PS-3003")]
        [Scenario(DisplayName = "Bad request returned if AddLoanRequest has invalid LoanManagerId")]
        [ScenarioCategory("Validation")]
        public async Task Bad_request_returned_if_AddLoanRequest_has_invalid_LoanManagerId()
        {
            var loanManagerId = _fixture.Create<int>();

            await Runner.WithContext<LoanContext>()
                .AddAsyncSteps(
                    
                    _ => _.Given_AddLoanRequest_with_invalid_LoanManagerId(loanManagerId),
                    
                    _ => _.When_invalid_request_is_received(),
                    
                    _ => _.Then_Bad_Request_response_is_returned_with_error_message(
                        string.Format(AddLoanRequestValidator.FormatInvalidIdForEntity, loanManagerId, nameof(LoanManager)), 0)
                )
                .RunAsync();
        }

        [Label("PS-3004")]
        [Scenario(DisplayName = "Bad request returned if AddLoanRequest has invalid loan values")]
        [ScenarioCategory("Validation")]
        [InlineData("DurationWeeks", 4, "The Loan Duration cannot be less than 10 weeks")]
        [InlineData("InterestRate", 1.1, "The Loan Interest Rate cannot be less than 1.4%")]
        [InlineData("Value", 90, "The Loan Value cannot be less than £100")]
        public async Task Bad_request_returned_if_AddLoanRequest_has_invalid_loan_values(string propertyName,
            object propertyValue, string errorMessage)
        {
            await Runner.WithContext<LoanContext>()
                .AddAsyncSteps(
                    
                    _ => _.Given_AddLoanRequest_with_invalid_property(propertyName, propertyValue),
                    
                    _ => _.When_invalid_request_is_received(),
                    
                    _ => _.Then_Bad_Request_response_is_returned_with_error_message(errorMessage, 0)
                )
                .RunAsync();
        }
    }
}