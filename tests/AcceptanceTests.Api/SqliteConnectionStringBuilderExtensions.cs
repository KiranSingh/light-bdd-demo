using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using AcroLoans.Models;
using AcroLoans.Repositories;
using AcroLoans.Repositories.Entities;
using Api;
using AutoFixture;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AcroLoans.AcceptanceTests.Api
{
    public static class SqliteConnectionStringBuilderExtensions
    {
        private static readonly Fixture _fixture = new Fixture();

        public static async Task<List<Loan>> LoansFromDatabase(this SqliteConnectionStringBuilder builder)
        {
            var result = new List<Loan>();
            await using var connection = new SqliteConnection(builder.ConnectionString);
            await connection.OpenAsync();

            await using var command = connection.CreateCommand();
            command.CommandText = $"SELECT * FROM LOAN";
            await using var reader = await command.ExecuteReaderAsync(CommandBehavior.CloseConnection);
            while (await reader.ReadAsync())
            {
                result.Add(new Loan
                {
                    Id = reader.GetInt32(0),
                    Deleted = reader.GetBoolean(1),
                    DurationWeeks = reader.GetInt32(2),
                    InterestRate = reader.GetDouble(3),
                    StartDate = reader.GetDateTime(4),
                    Value = reader.GetDouble(5),
                    CustomerId = reader.GetInt32(6),
                    LoanManagerId = reader.GetInt32(7),
                });
            }

            return result;
        }
        
        public static async Task<TestServer> TestServerWithData(this SqliteConnectionStringBuilder builder,
            IConfiguration configurationRoot, int customerId = 0, int loanManagerId = 0)
        {
            var testServer = TestServer(builder, configurationRoot);

            using var scope = testServer.Host.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<AcroLoansContext>();

            await context.Database.OpenConnectionAsync();
            await context.Database.EnsureCreatedAsync();

            context.ClearTables();
                
            if (customerId > 0)
            {
                await context.Customers.AddAsync(
                    _fixture.Build<CustomerEntity>()
                        .With(x => x.Id, customerId)
                        .Without(x => x.Loans)
                        .Create());
            }

            if (loanManagerId > 0)
            {
                await context.LoanManagers.AddAsync(
                    _fixture.Build<LoanManagerEntity>()
                        .With(x => x.Id, loanManagerId)
                        .Without(x => x.Loans)
                        .Create());
            }
            await context.SaveChangesAsync();

            return testServer;
        }

        public static TestServer TestServer(this SqliteConnectionStringBuilder builder, IConfiguration configurationRoot)
        {
            return new TestServer(new WebHostBuilder()
                .ConfigureServices(services =>
                {
                    try
                    {
                        var connection = new SqliteConnection(builder.ConnectionString);
                        connection.Open();
                        connection.EnableExtensions(true);

                        services.AddEntityFrameworkSqlite()
                            .AddDbContext<AcroLoansContext>(options => options.UseSqlite(connection));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                })
                .UseConfiguration(configurationRoot)
                .UseStartup<Startup>()
            );
        }
    }
}