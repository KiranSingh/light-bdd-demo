
using Microsoft.Extensions.Configuration;

namespace AcroLoans.Api.AcceptanceTests
{
    public class ConfigHelper
    {
        public static IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile("appsettings.Development.json", false, true)
                .Build();
        }
    }
}