using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using AcroLoans.Repositories;
using Api;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Newtonsoft.Json;

namespace AcroLoans.Api.AcceptanceTests
{
    public class LoanContext
    {
        private readonly Fixture _fixture = new Fixture();

        private AddLoanRequest _addLoanRequest;
        private readonly Mock<IRepository<Customer>> _customerRepository;
        private readonly Mock<IRepository<Loan>> _loanRepository;
        private readonly Mock<IRepository<LoanManager>> _loanManagerRepository;
        private HttpResponseMessage _response;
        private readonly Mock<LoanManager> _loanManager;
        private readonly LoanConfiguration _loanConfiguration;
        private readonly IConfigurationRoot _configurationRoot;
        private readonly int _loanId;

        public LoanContext()
        {
            _customerRepository = new Mock<IRepository<Customer>>();
            _loanRepository = new Mock<IRepository<Loan>>();
            _loanManagerRepository = new Mock<IRepository<LoanManager>>();

            _configurationRoot = ConfigHelper.GetConfiguration();
            _loanConfiguration = new LoanConfiguration();
            _configurationRoot.Bind("Loan", _loanConfiguration);

            _loanManager = new Mock<LoanManager>();

            _loanId = _fixture.Create<int>();
        }

        public void Given_valid_AddLoanRequest()
        {
            _addLoanRequest = new AddLoanRequest
            {
                CustomerId = _fixture.Create<int>(),
                DurationWeeks = _loanConfiguration.MinLoanDurationWeeks + _fixture.Create<int>(),
                InterestRate = _loanConfiguration.MinLoanInterestRate + _fixture.Create<double>(),
                LoanManagerId = _fixture.Create<int>(),
                StartDate = DateTime.Today.AddDays(_fixture.Create<int>()),
                Value = _loanConfiguration.MinLoanValue + _fixture.Create<double>(),
            };
        }

        public async Task When_request_is_received()
        {
            var testServer = new TestServer(new WebHostBuilder()
                .ConfigureServices(services =>
                {
                    services.AddScoped<AcroLoansContext>(sp => new Mock<AcroLoansContext>().Object);

                    services.AddScoped(sp => _customerRepository.Object);
                    services.AddScoped(sp => _loanRepository.Object);
                    services.AddScoped<IReadOnlyRepository<LoanManager>>(sp => _loanManagerRepository.Object);
                })
                .UseConfiguration(_configurationRoot)
                .UseStartup<Startup>()
            );

            _customerRepository.Setup(x => x.Exists(_addLoanRequest.CustomerId))
                .ReturnsAsync(true);
            _loanManagerRepository.Setup(x => x.Exists(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(true);

            _loanManager.Setup(x => x.AllowedToAddLoanFor(
                    It.Is<CustomerValidationModel>(y =>
                        y.CustomerId == _addLoanRequest.CustomerId &&
                        y.LoanManagerId == _addLoanRequest.LoanManagerId &&
                        y.MaxActiveCustomersAllowed == _loanConfiguration.MaxActiveCustomersAllowed
                    )))
                .Returns(true);
            _loanManagerRepository.Setup(x => x.Get(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(_loanManager.Object);

            _loanRepository.Setup(x => x.Add(It.IsAny<Loan>()))
                .ReturnsAsync(_loanId);

            var client = testServer.CreateClient();

            _response = await client.PostAsJsonAsync("/api/loans", _addLoanRequest);
        }

        public void Then_new_loan_added_for_customer()
        {
            _customerRepository.Verify(x => x.Exists(_addLoanRequest.CustomerId));

            _loanManagerRepository.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));

            _loanManagerRepository.Verify(x => x.Get(_addLoanRequest.LoanManagerId));

            _loanManager.Verify(x => x.AllowedToAddLoanFor(
                It.Is<CustomerValidationModel>(y =>
                    y.CustomerId == _addLoanRequest.CustomerId &&
                    y.LoanManagerId == _addLoanRequest.LoanManagerId &&
                    y.MaxActiveCustomersAllowed == _loanConfiguration.MaxActiveCustomersAllowed
                )));

            _loanRepository.Verify(x => x.Add(It.IsAny<Loan>()));
        }

        public async Task Then_valid_loan_created_response_is_returned()
        {
            _response.StatusCode.Should().Be(HttpStatusCode.Created);

            _response.Headers.Location.AbsoluteUri.ToLower().Should().Be($"http://localhost/api/loans/{_loanId}");

            var content = await _response.Content.ReadAsStringAsync();

            var loan = JsonConvert.DeserializeObject<Loan>(content);
            loan.Id.Should().Be(_loanId);
            loan.CustomerId.Should().Be(_addLoanRequest.CustomerId);
            loan.Deleted.Should().BeFalse();
            loan.DurationWeeks.Should().Be(_addLoanRequest.DurationWeeks);
            loan.InterestRate.Should().Be(_addLoanRequest.InterestRate);
            loan.LoanManagerId.Should().Be(_addLoanRequest.LoanManagerId);
            loan.StartDate.Should().Be(_addLoanRequest.StartDate);
            loan.Value.Should().Be(_addLoanRequest.Value);
        }

        public void Given_AddLoanRequest_with_invalid_CustomerId(int customerId)
        {
            Given_valid_AddLoanRequest();
            _addLoanRequest.CustomerId = customerId;

            _customerRepository.Setup(x => x.Exists(_addLoanRequest.CustomerId))
                .ReturnsAsync(false);
        }

        public async Task When_invalid_request_is_received()
        {
            var testServer = new TestServer(new WebHostBuilder()
                .ConfigureServices(services =>
                {
                    services.AddScoped<AcroLoansContext>(sp => new Mock<AcroLoansContext>().Object);

                    services.AddScoped(sp => _customerRepository.Object);
                    services.AddScoped(sp => _loanRepository.Object);
                    services.AddScoped<IReadOnlyRepository<LoanManager>>(sp => _loanManagerRepository.Object);
                })
                .UseConfiguration(ConfigHelper.GetConfiguration())
                .UseStartup<Startup>()
            );

            var client = testServer.CreateClient();

            _response = await client.PostAsJsonAsync("/api/loans", _addLoanRequest);
        }

        public void Then_Bad_Request_response_is_returned_with_CustomerId_error_message(string errorMessage)
        {
            _customerRepository.Verify(x => x.Exists(_addLoanRequest.CustomerId));

            _loanManagerRepository.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));

            _loanManagerRepository.Verify(x => x.Get(_addLoanRequest.LoanManagerId), Times.Never);

            _loanRepository.Verify(x => x.Add(It.IsAny<Loan>()), Times.Never);

            _response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var response = _response.Content.ReadAsStringAsync().Result;
            response.Should().Contain(errorMessage);
        }

        public void Given_AddLoanRequest_with_invalid_LoanManagerId(int loanManagerId)
        {
            Given_valid_AddLoanRequest();
            _addLoanRequest.LoanManagerId = loanManagerId;

            _customerRepository.Setup(x => x.Exists(_addLoanRequest.CustomerId))
                .ReturnsAsync(true);
            _loanManagerRepository.Setup(x => x.Exists(loanManagerId))
                .ReturnsAsync(false);
        }

        public void Then_Bad_Request_response_is_returned_with_LoanManagerId_error_message(string errorMessage)
        {
            _customerRepository.Verify(x => x.Exists(_addLoanRequest.CustomerId));

            _loanManagerRepository.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));

            _loanManagerRepository.Verify(x => x.Get(_addLoanRequest.LoanManagerId), Times.Never);

            _loanRepository.Verify(x => x.Add(It.IsAny<Loan>()), Times.Never);

            _response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var response = _response.Content.ReadAsStringAsync().Result;
            response.Should().Contain(errorMessage);
        }

        public void Given_AddLoanRequest_with_invalid_property(string propertyName, object propertyValue)
        {
            Given_valid_AddLoanRequest();
            typeof(AddLoanRequest).GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public)
                .SetValue(_addLoanRequest, propertyValue);
            
            _customerRepository.Setup(x => x.Exists(_addLoanRequest.CustomerId))
                .ReturnsAsync(true);
            _loanManagerRepository.Setup(x => x.Exists(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(true);

            _loanManager.Setup(x => x.AllowedToAddLoanFor(
                    It.Is<CustomerValidationModel>(y =>
                        y.CustomerId == _addLoanRequest.CustomerId &&
                        y.LoanManagerId == _addLoanRequest.LoanManagerId &&
                        y.MaxActiveCustomersAllowed == _loanConfiguration.MaxActiveCustomersAllowed
                    )))
                .Returns(true);
            _loanManagerRepository.Setup(x => x.Get(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(_loanManager.Object);
        }
        

        public void Then_Bad_Request_response_is_returned_with_error_message(string errorMessage)
        {
            _customerRepository.Verify(x => x.Exists(_addLoanRequest.CustomerId));

            _loanManagerRepository.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));

            _loanManagerRepository.Verify(x => x.Get(_addLoanRequest.LoanManagerId));
            
            _loanManager.Verify(x => x.AllowedToAddLoanFor(
                It.Is<CustomerValidationModel>(y =>
                    y.CustomerId == _addLoanRequest.CustomerId &&
                    y.LoanManagerId == _addLoanRequest.LoanManagerId &&
                    y.MaxActiveCustomersAllowed == _loanConfiguration.MaxActiveCustomersAllowed
                )));

            _loanRepository.Verify(x => x.Add(It.IsAny<Loan>()), Times.Never);

            _response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var response = _response.Content.ReadAsStringAsync().Result;
            response.Should().Contain(errorMessage);
        }

        public void Given_AddLoanRequest_for_LoanManager_fully_extended(int loanManagerId)
        {
            Given_valid_AddLoanRequest();
            _addLoanRequest.LoanManagerId = loanManagerId;
            
            _customerRepository.Setup(x => x.Exists(_addLoanRequest.CustomerId))
                .ReturnsAsync(true);
            _loanManagerRepository.Setup(x => x.Exists(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(true);

            _loanManager.Setup(x => x.AllowedToAddLoanFor(
                    It.Is<CustomerValidationModel>(y =>
                        y.CustomerId == _addLoanRequest.CustomerId &&
                        y.LoanManagerId == _addLoanRequest.LoanManagerId &&
                        y.MaxActiveCustomersAllowed == _loanConfiguration.MaxActiveCustomersAllowed
                    )))
                .Returns(false);
            
            _loanManagerRepository.Setup(x => x.Get(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(_loanManager.Object);
        }
    }
}