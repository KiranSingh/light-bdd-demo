using System;
using System.Linq;
using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using AcroLoans.Services;
using Api.Validators;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace AcroLoans.Api.UnitTests
{
    public class AddLoanRequestValidatorTests
    {
        private readonly Fixture _fixture = new Fixture();
        
        private AddLoanRequestValidator _validator;
        private Mock<IService<Customer>> _customerService;
        private Mock<ILoanManagerService> _loanManagerService;
        private Mock<IMapper> _mapper;
        private LoanConfiguration _config;
        private AddLoanRequest _addLoanRequest;
        private CustomerValidationModel _customerValidationModel;

        [SetUp]
        public void SetUp()
        {
            _config = _fixture.Create<LoanConfiguration>();
            _addLoanRequest = new AddLoanRequest
            {
                 CustomerId = _fixture.Create<int>(),
                 DurationWeeks = _config.MinLoanDurationWeeks + _fixture.Create<int>(),
                 InterestRate = _config.MinLoanInterestRate + _fixture.Create<double>(),
                 LoanManagerId = _fixture.Create<int>(),
                 StartDate = DateTime.Today.AddDays(_fixture.Create<int>()),
                 Value = _config.MinLoanValue + _fixture.Create<double>(),
            };
            
            _mapper = new Mock<IMapper>();

            _customerService = new Mock<IService<Customer>>();
            _customerService.Setup(x => x.Exists(_addLoanRequest.CustomerId))
                .ReturnsAsync(true);

            _loanManagerService = new Mock<ILoanManagerService>();
            _loanManagerService.Setup(x => x.Exists(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(true);
            
            _customerValidationModel = new Mock<CustomerValidationModel>().Object;
            _mapper.Setup(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest))
                .Returns(_customerValidationModel);
            _loanManagerService.Setup(x => x.AllowedToAddLoanFor(_customerValidationModel))
                .ReturnsAsync(true);

            _validator = new AddLoanRequestValidator(_customerService.Object, _loanManagerService.Object, _mapper.Object, _config);
        }

        [Test]
        public async Task Validate_ValidModel_IsValidReturnsTrue()
        {
            // Arrange
            // Act
            var actual = await _validator.ValidateAsync(_addLoanRequest);
            
            // Assert
            _customerService.Verify(x => x.Exists(_addLoanRequest.CustomerId));
            _loanManagerService.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));
            _mapper.Verify(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest));
            _loanManagerService.Verify(x => x.AllowedToAddLoanFor(_customerValidationModel));
            actual.IsValid.Should().BeTrue();
        }

        [Test]
        public async Task Validate_DurationLessThanMinRequired_IsValidReturnsFalse()
        {
            // Arrange
            var expected = $"The Loan Duration cannot be less than {_config.MinLoanDurationWeeks} weeks";
            _addLoanRequest.DurationWeeks = _config.MinLoanDurationWeeks - 1;

            // Act
            var actual = await _validator.ValidateAsync(_addLoanRequest);

            // Assert
            _customerService.Verify(x => x.Exists(_addLoanRequest.CustomerId));
            _loanManagerService.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));
            _mapper.Verify(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest));
            _loanManagerService.Verify(x => x.AllowedToAddLoanFor(_customerValidationModel));
            actual.IsValid.Should().BeFalse();
            actual.Errors.First().ErrorMessage.Should().Be(expected);
        }

        [Test]
        public async Task Validate_InterestRateLessThanMinRequired_IsValidReturnsFalse()
        {
            // Arrange
            var expected = $"The Loan Interest Rate cannot be less than {_config.MinLoanInterestRate}%";
            _addLoanRequest.InterestRate = _config.MinLoanInterestRate - 0.1;

            // Act
            var actual = await _validator.ValidateAsync(_addLoanRequest);

            // Assert
            _customerService.Verify(x => x.Exists(_addLoanRequest.CustomerId));
            _loanManagerService.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));
            _mapper.Verify(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest));
            _loanManagerService.Verify(x => x.AllowedToAddLoanFor(_customerValidationModel));
            actual.IsValid.Should().BeFalse();
            actual.Errors.First().ErrorMessage.Should().Be(expected);
        }

        [Test]
        public async Task Validate_ValueLessThanMinRequired_IsValidReturnsFalse()
        {
            // Arrange
            var expected = $"The Loan Value cannot be less than £{_config.MinLoanValue}";
            _addLoanRequest.Value = _config.MinLoanValue - 2;

            // Act
            var actual = await _validator.ValidateAsync(_addLoanRequest);

            // Assert
            _customerService.Verify(x => x.Exists(_addLoanRequest.CustomerId));
            _loanManagerService.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));
            _mapper.Verify(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest));
            _loanManagerService.Verify(x => x.AllowedToAddLoanFor(_customerValidationModel));
            actual.IsValid.Should().BeFalse();
            actual.Errors.First().ErrorMessage.Should().Be(expected);
        }

        [Test]
        public async Task Validate_LoanManagerNotAllowedToTakeCustomer_IsValidReturnsFalse()
        {
            // Arrange
            var expected = string.Format(AddLoanRequestValidator.FormatLoanManagerFullyExtended, _addLoanRequest.LoanManagerId);
            _loanManagerService.Setup(x => x.AllowedToAddLoanFor(_customerValidationModel))
                .ReturnsAsync(false);

            // Act
            var actual = await _validator.ValidateAsync(_addLoanRequest);

            // Assert
            _customerService.Verify(x => x.Exists(_addLoanRequest.CustomerId));
            _loanManagerService.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));
            _mapper.Verify(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest));
            _loanManagerService.Verify(x => x.AllowedToAddLoanFor(_customerValidationModel));
            actual.IsValid.Should().BeFalse();
            actual.Errors.First().ErrorMessage.Should().Be(expected);
        }

        [Test]
        public async Task Validate_LoanManagerIdNotValid_IsValidReturnsFalse()
        {
            // Arrange
            var expected = string.Format(AddLoanRequestValidator.FormatInvalidIdForEntity,
                _addLoanRequest.LoanManagerId, nameof(LoanManager));
            _loanManagerService.Setup(x => x.Exists(_addLoanRequest.LoanManagerId))
                .ReturnsAsync(false);

            // Act
            var actual = await _validator.ValidateAsync(_addLoanRequest);

            // Assert
            _customerService.Verify(x => x.Exists(_addLoanRequest.CustomerId));
            _loanManagerService.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));
            _mapper.Verify(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest), Times.Never);
            _loanManagerService.Verify(x => x.AllowedToAddLoanFor(It.IsAny<CustomerValidationModel>()), Times.Never);
            actual.IsValid.Should().BeFalse();
            actual.Errors.Count.Should().Be(1);
            actual.Errors.First().ErrorMessage.Should().Be(expected);
        }

        [Test]
        public async Task Validate_CustomerIdNotValid_IsValidReturnsFalse()
        {
            // Arrange
            var expected = string.Format(AddLoanRequestValidator.FormatInvalidIdForEntity, _addLoanRequest.CustomerId,
                nameof(Customer));
            _customerService.Setup(x => x.Exists(_addLoanRequest.CustomerId))
                .ReturnsAsync(false);

            // Act
            var actual = await _validator.ValidateAsync(_addLoanRequest);

            // Assert
            _customerService.Verify(x => x.Exists(_addLoanRequest.CustomerId));
            _loanManagerService.Verify(x => x.Exists(_addLoanRequest.LoanManagerId));
            _mapper.Verify(x => x.Map<AddLoanRequest, CustomerValidationModel>(_addLoanRequest), Times.Never);
            _loanManagerService.Verify(x => x.AllowedToAddLoanFor(It.IsAny<CustomerValidationModel>()), Times.Never);
            actual.IsValid.Should().BeFalse();
            actual.Errors.Count.Should().Be(1);
            actual.Errors.First().ErrorMessage.Should().Be(expected);
        }
    }
}