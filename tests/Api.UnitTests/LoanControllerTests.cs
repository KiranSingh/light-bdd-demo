using System.Reflection;
using System.Threading.Tasks;
using AcroLoans.Common;
using AcroLoans.Models;
using Api.Controllers;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace AcroLoans.Api.UnitTests
{
    public class LoanControllerTests
    {
        private readonly Fixture _fixture = new Fixture();
        
        private LoansController _controller;
        private Mock<IMapper> _mapper;
        private Mock<IService<Loan>> _service;

        [SetUp]
        public void SetUp()
        {
            _mapper = new Mock<IMapper>();
            _service = new Mock<IService<Loan>>();
            _controller = new LoansController(_mapper.Object, _service.Object);
        }

        [Test]
        public void Class_RouteAttribute_ExpectedTemplate()
        {
            typeof(LoansController)
                .GetCustomAttribute<RouteAttribute>()
                .Template.Should().Be("api/[controller]");
        }

        [Test]
        public async Task Get_ValidIdInput_ReturnsResultFromService()
        {
            // Arrange
            var expected = new Mock<Loan>().Object;
            var id = _fixture.Create<int>();
            _service.Setup(x => x.Get(id))
                .ReturnsAsync(expected);

            // Act
            var actual = await _controller.Get(id);
            
            // Assert
            _service.Verify(x => x.Get(id));
            actual.As<OkObjectResult>().Value.Should().Be(expected);
        }
        
        [TestCase(-7)]
        [TestCase(0)]
        public async Task Get_InvalidId_ReturnsBadRequestResult(int id)
        {
            // Arrange
            var expected = LoansController.ErrorMessageValidIdRequired;
            
            // Act
            var actual = await _controller.Get(id);
            
            // Assert
            _service.Verify(x => x.Get(id), Times.Never);
            actual.As<BadRequestObjectResult>().Value.Should().Be(expected);
        }
        
        [Test]
        public async Task Post_ModelStateValid_MapsRequestToModel_AddsModelToService_ReturnsCreatedResultWithUpdatedModel()
        {
            // Arrange
            var request = new Mock<AddLoanRequest>().Object;
            var loan = new Mock<Loan>();
            _mapper.Setup(x => x.Map<AddLoanRequest, Loan>(request))
                .Returns(loan.Object);

            var expected = _fixture.Create<int>();
            _service.Setup(x => x.Add(loan.Object))
                .ReturnsAsync(expected);

            // Act
            var actual = await _controller.Post(request);
            
            // Assert
            _mapper.Verify(x => x.Map<AddLoanRequest, Loan>(request));
            _service.Verify(x => x.Add(loan.Object));
            var result = actual.As<CreatedAtActionResult>();
            result.Should().NotBeNull();
            result.ControllerName.Should().BeNullOrEmpty();
            result.ActionName.Should().Be(nameof(LoansController.Get));
            result.RouteValues["id"].Should().Be(expected);
            loan.VerifySet(x => x.Id = expected);
            result.Value.Should().Be(loan.Object);
        }
        
        [Test]
        public async Task Post_ModelStateNotValid_ReturnsBadRequestResult()
        {
            // Arrange
            var request = new Mock<AddLoanRequest>().Object;
            _controller.ModelState.AddModelError("someProperty", "someError");

            // Act
            var actual = await _controller.Post(request);
            
            // Assert
            _mapper.Verify(x => x.Map<AddLoanRequest, Loan>(request), Times.Never);
            _service.Verify(x => x.Add(It.IsAny<Loan>()), Times.Never);
            var result = actual.As<BadRequestObjectResult>();
            result.Should().NotBeNull();
            result.Value.As<SerializableError>()["someProperty"]
                .As<string[]>()[0].Should().Be("someError");
        }
    }
}