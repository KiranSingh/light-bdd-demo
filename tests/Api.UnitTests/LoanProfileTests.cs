using System;
using AcroLoans.Common;
using AcroLoans.Models;
using Api.Mappings;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace AcroLoans.Api.UnitTests
{
    public class LoanProfileTests
    {
        private readonly Fixture _fixture = new Fixture();
        private LoanConfiguration _loanConfiguration;
        private IMapper _mapper;

        [SetUp]
        public void SetUp()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<LoanProfile>());
            mapperConfiguration.AssertConfigurationIsValid();

            _loanConfiguration = _fixture.Create<LoanConfiguration>();
            
            var serviceCtor = new Mock<IServiceProvider>();
            serviceCtor.Setup(x => x.GetService(typeof(MaxActiveCustomersAllowedResolver)))
                .Returns(new MaxActiveCustomersAllowedResolver(_loanConfiguration));
            _mapper = mapperConfiguration.CreateMapper(serviceCtor.Object.GetService);
        }

        [Test]
        public void Map_AddLoanRequestToCustomerValidationModel_ResultAsExpected()
        {
            // Arrange
            var input = _fixture.Create<AddLoanRequest>();
            
            // Act
            var actual = _mapper.Map<AddLoanRequest, CustomerValidationModel>(input);
            
            // Assert
            actual.CustomerId.Should().Be(input.CustomerId);
            actual.LoanManagerId.Should().Be(input.LoanManagerId);
            actual.MaxActiveCustomersAllowed.Should().Be(_loanConfiguration.MaxActiveCustomersAllowed);
        }
        
        [Test]
        public void Map_AddLoanRequestToLoan_ResultAsExpected()
        {
            // Arrange
            var input = _fixture.Create<AddLoanRequest>();

            // Act
            var actual = _mapper.Map<AddLoanRequest, Loan>(input);
            
            // Assert
            actual.Id.Should().Be(0);
            actual.CustomerId.Should().Be(input.CustomerId);
            actual.Deleted.Should().BeFalse();
            actual.DurationWeeks.Should().Be(input.DurationWeeks);
            actual.InterestRate.Should().Be(input.InterestRate);
            actual.LoanManagerId.Should().Be(input.LoanManagerId);
            actual.StartDate.Should().Be(input.StartDate);
            actual.Value.Should().Be(input.Value);
        }
    }
}