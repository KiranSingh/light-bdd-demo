using System;
using FluentAssertions;
using NUnit.Framework;

namespace AcroLoans.Models.Tests
{
    public class AddLoanRequestTests
    {
        [Test]
        public void StartDate_NewObject_ReturnsTodaysDate()
        {
            // Arrange
            // Act
            var actual = new AddLoanRequest().StartDate;
            
            // Assert
            actual.Should().Be(DateTime.Today);
        }
    }
}