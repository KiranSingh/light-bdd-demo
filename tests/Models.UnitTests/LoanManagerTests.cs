using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace AcroLoans.Models.Tests
{
    public class LoanManagerTests
    {
        private readonly Fixture _fixture = new Fixture();
        private LoanManager _loanManager;
        private Mock<LoanManager> _loanManagerMock;

        [SetUp]
        public void SetUp()
        {
            _loanManager = new LoanManager
            {
                Loans = new List<Loan>(),
            };
            
            _loanManagerMock = new Mock<LoanManager>
            {
                CallBase = true,
            };
        }

        [Test]
        public void ActiveCustomerIds_LoanActive_ReturnsDistinctCustomerIds()
        {
            // Arrange
            var expected = _fixture.Create<List<int>>();
            expected.ForEach(x =>
            {
                var mocks = Enumerable.Range(1, new Random().Next(5, 10)).ToList()
                                .Select(y => new Mock<Loan>()).ToList();
                for (int i = 0; i < mocks.Count; i++)
                {
                    mocks[i].Setup(y => y.Active()).Returns(i % 2 == 0);
                    mocks[i].SetupGet(y => y.CustomerId).Returns(x);
                }
                mocks.ForEach(y => _loanManager.Loans.Add(y.Object));
            });

            var expiredLoan = new Mock<Loan>();
            var expiredLoanId = _fixture.Create<int>();
            while (expected.Contains(expiredLoanId))
            {
                expiredLoanId = _fixture.Create<int>();
            }
                
            expiredLoan.Setup(x => x.Active())
                .Returns(false);
            expiredLoan.SetupGet(x => x.CustomerId)
                .Returns(expiredLoanId);
            _loanManager.Loans.Add(expiredLoan.Object);

            // Act
            var actual = _loanManager.ActiveCustomerIds;

            // Assert
            actual.Should().BeEquivalentTo(expected);
        }
        
        [Test]
        public void AllowedToAddLoanFor_ActiveCustomersLessThanMax_ReturnsTrue()
        {
            // Arrange
            var activeCustomerIds = _fixture.Create<List<int>>();
            _loanManagerMock.SetupGet(x => x.ActiveCustomerIds)
                .Returns(activeCustomerIds);
            
            var model = _fixture.Create<CustomerValidationModel>();
            model.MaxActiveCustomersAllowed = activeCustomerIds.Count + 1;

            // Act
            var actual = _loanManagerMock.Object.AllowedToAddLoanFor(model);
            
            // Assert
            actual.Should().BeTrue();
        }
        
        [Test]
        public void AllowedToAddLoanFor_ActiveCustomersMoreThanMax_CustomerIdInActiveCustomers_ReturnsTrue()
        {
            // Arrange
            var activeCustomerIds = _fixture.Create<List<int>>();
            _loanManagerMock.SetupGet(x => x.ActiveCustomerIds)
                .Returns(activeCustomerIds);
            
            var model = _fixture.Create<CustomerValidationModel>();
            
            activeCustomerIds.Add(model.CustomerId);
            model.MaxActiveCustomersAllowed = activeCustomerIds.Count - 1;

            // Act
            var actual = _loanManagerMock.Object.AllowedToAddLoanFor(model);
            
            // Assert
            actual.Should().BeTrue();
        }

        [Test]
        public void AllowedToAddLoanFor_ActiveCustomersMoreThanMax_CustomerIdNotInActiveCustomers_ReturnsFalse()
        {
            // Arrange
            var activeCustomerIds = _fixture.Create<List<int>>();
            _loanManagerMock.SetupGet(x => x.ActiveCustomerIds)
                .Returns(activeCustomerIds);
            
            var model = _fixture.Create<CustomerValidationModel>();
            while (activeCustomerIds.Contains(model.CustomerId))
            {
                model.CustomerId = _fixture.Create<int>();
            }
            
            model.MaxActiveCustomersAllowed = activeCustomerIds.Count - 1;

            // Act
            var actual = _loanManagerMock.Object.AllowedToAddLoanFor(model);
            
            // Assert
            actual.Should().BeFalse();
        }

        [Test]
        public void AllowedToAddLoanFor_NoActiveCustomersIds_ReturnsTrue()
        {
            // Arrange
            _loanManager.Loans.Should().BeNullOrEmpty();
            _loanManager.ActiveCustomerIds.Should().BeNullOrEmpty();

            // Act
            var actual = _loanManager.AllowedToAddLoanFor(_fixture.Create<CustomerValidationModel>());
            
            // Assert
            actual.Should().BeTrue();
        }
    }
}