using System;
using AutoFixture;
using FluentAssertions;
using NUnit.Framework;

namespace AcroLoans.Models.Tests
{
    public class LoanTests
    {
        private readonly Fixture _fixture = new Fixture();
        private Loan _loan;

        [SetUp]
        public void SetUp()
        {
            var days = _fixture.Create<int>();
            _loan = new Loan
            {
                StartDate = DateTime.Now.AddDays(-days),
                DurationWeeks = days/7 + 8,
            };
        }

        [Test]
        public void Active_TodayBetweenStartAndEndDates_ReturnsTrue()
        {
            // Arrange
            // Act
            var actual = _loan.Active();
            
            // Assert
            actual.Should().BeTrue();
        }

        [Test]
        public void Active_StartDateOfNow_ReturnsTrue()
        {
            // Arrange
            _loan.StartDate = DateTime.Now;
            
            // Act
            var actual = _loan.Active();
            
            // Assert
            actual.Should().BeTrue();
        }

        [Test]
        public void Active_StartDateInFuture_ReturnsFalse()
        {
            // Arrange
            _loan.StartDate = DateTime.Now.AddDays(5);
            
            // Act
            var actual = _loan.Active();
            
            // Assert
            actual.Should().BeFalse();
        }
        
        [Test]
        public void Active_EndDateInPast_ReturnsFalse()
        {
            // Arrange
            _loan.StartDate = DateTime.Now.AddDays(-75);
            _loan.DurationWeeks = 4;
            
            // Act
            var actual = _loan.Active();
            
            // Assert
            actual.Should().BeFalse();
        }
    }
}